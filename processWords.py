#Author: JingCheng Wang @ Unimelb
import editdistance
import ngram
import distance
import sys

dictionary = open("data/dict.txt", "r")
dict_lines = dictionary.readlines()
dictionary.close()



def valid_token(token):
    symbol = "!#:().<>,=?"
    for char in symbol:
        if token.__contains__(char):
            return False

    return True

# Using edit distance as the matching metric


def find_best_match_using_ed(token):
    min_val = sys.maxint
    best_match = "No Match"
    for line in dict_lines:
        val = editdistance.eval(token, line.strip())
        if val == 0:
            return [line.strip(), 0]
        elif val < min_val:
            min_val = val
            best_match = line.strip()

    return best_match


def find_best_match_using_ngram(token,n):
    max_val = 0
    best_match = "No Match"
    for line in dict_lines:
        val = ngram.NGram.compare(token, line.strip(),N=2)
        if val == 1:
            return line.strip()
        elif val > max_val:
            max_val = val
            best_match = line.strip()

    return best_match

# Longest Common Substring Distance


def find_best_match_using_lcsd(token):
    max_val = 0
    best_match = "No Match"
    complete_match_list=[]
    for line in dict_lines:
        substrings = list(distance.lcsubstrings(token, line.strip()))

        if not substrings:
            continue
        else:
            max_length = len(max(substrings,key=len))

        if max_length == len(token):
            complete_match_list.append(line.strip())
        elif max_length > max_val:
            max_val = max_length
            best_match = line.strip()

    if not complete_match_list:
        return best_match
    else:
        return min(complete_match_list,key=len)
