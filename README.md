### What is this repository for? ###

The source code Knowledge Technology Assignment 1

### How do I get set up? ###

Run the code using "python matchToken.py",the python version I used is 2.6, the follow the prompt to perform tasks on
the data.All the data should be put into a 'data' directory.

The program will use one of the 3 techniques to analyse the data file "labled-tokens.txt" based on the user's input.
after finishing the matching, an accuracy report will be showing on stdout.

Additional installation with using "pip" may be needed.
The packages I used can be installed with the following commands:

pip install editdistance
pip install distance
pip install ngram


### Disclaimer ###

I don't own the any code in the import packages "editdistance" "distance" and "ngram" , they are imported to this
project to help the matching process.

