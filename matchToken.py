#Author: JingCheng Wang @ Unimelb
from __future__ import print_function

import processWords

# some constants
EDIT_DISTANCE = 1
N_GRAM = 2
LCSD = 3

# ready the input file
inputFile = open("data/labelled-tokens.txt", "r")

def start_matching(metrics,n,match_option):
    num_of_correct_match = 0
    num_of_token = 0
    for line in inputFile:
        original_word = line.split()[0]
        word_type = line.split()[1]
        target_word = line.split()[2]

        if match_option != 'Y' and word_type!= 'OOV':
            continue



        if metrics == EDIT_DISTANCE:
            best_match = processWords.find_best_match_using_ed(original_word)
        elif metrics == N_GRAM:
            best_match = processWords.find_best_match_using_ngram(original_word,n)
        else:
            best_match = processWords.find_best_match_using_lcsd(original_word)

        if best_match == target_word:
            print("Match successful")
            num_of_correct_match += 1

        num_of_token += 1
        print("The original word is \'", original_word, "\' The best match is \'", best_match, "\'")

    return [num_of_correct_match, num_of_token]


def main():

    option = raw_input("Please choose a metric "
                       "for string matching,Enter '1' for Edit distance, '2' for N-gram,'3' for LCSD\n")

    if int(option) == N_GRAM:
        n = raw_input("You chose N-gram,Please Enter N\n")
        if n == '':
            print("unspecified n,exit")
            exit(0)


    n=0
    match_option = raw_input("Please choose if you want to match all tokens?(Y/N)\n")

    result = start_matching(int(option), n, match_option)

    print("Number of token processed: ", result[1])
    print("Number of correct match found: ", result[0])
    print("Correct Percentage= ", result[0] * 1.0 / result[1])
    inputFile.close()


if __name__ == '__main__':
    main()
